#include "maindialog.h"
#include "ui_maindialog.h"

#include <QMessageBox>
#include <QNetworkInterface>

MainDialog::MainDialog(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::MainDialog)
{
    ui->setupUi(this);

    if (!mServer.listen(QHostAddress::Any, 45329)) {
        QMessageBox::critical(this, tr("Knowledge Check Server"),
                              tr("Unable to start the server: %1.")
                              .arg(mServer.errorString()));
        close();
        return;
    }

    QString ipAddress;
    QList<QHostAddress> ipAddressesList = QNetworkInterface::allAddresses();
    // use the first non-localhost IPv4 address
    for (int i = 0; i < ipAddressesList.size(); ++i) {
        if (ipAddressesList.at(i) != QHostAddress::LocalHost &&
            ipAddressesList.at(i).toIPv4Address()) {
            ipAddress = ipAddressesList.at(i).toString();
            break;
        }
    }
    // if we did not find one, use IPv4 localhost
    if (ipAddress.isEmpty())
        ipAddress = QHostAddress(QHostAddress::LocalHost).toString();
    ui->labStatus->setText(tr("The server is running on\n\nIP: %1\nport: %2\n\n")
                         .arg(ipAddress).arg(mServer.serverPort()));

    connect(ui->btQuit, &QPushButton::clicked, this, &MainDialog::close);
}

MainDialog::~MainDialog()
{
    delete ui;
}

