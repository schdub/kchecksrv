BEGIN TRANSACTION;

-- создаем необходимые таблицы

DROP TABLE IF EXISTS "question";
CREATE TABLE IF NOT EXISTS "question" (
	"id"	INTEGER,
	"text"	TEXT NOT NULL,
	"picture"	TEXT,
	"options"	TEXT NOT NULL,
	"valid"	TEXT NOT NULL,
	"type"	INTEGER,
	"test$id"	INTEGER,
	PRIMARY KEY("id")
);

DROP TABLE IF EXISTS "grp";
CREATE TABLE IF NOT EXISTS "grp" (
	"id"	INTEGER,
	"name"	TEXT NOT NULL UNIQUE,
	PRIMARY KEY("id")
);

DROP TABLE IF EXISTS "user";
CREATE TABLE IF NOT EXISTS "user" (
	"id"	INTEGER,
	"name"	TEXT NOT NULL UNIQUE,
	"password"	TEXT NOT NULL UNIQUE,
	"grp$id"	INTEGER,
	"blocked"	INTEGER DEFAULT 0,
	FOREIGN KEY("grp$id") REFERENCES "grp"("id"),
	PRIMARY KEY("id")
);

DROP TABLE IF EXISTS "test";
CREATE TABLE IF NOT EXISTS "test" (
	"id"	INTEGER,
	"name"	TEXT NOT NULL UNIQUE,
	"duration"	INTEGER NOT NULL,
	"score_excellent"	INTEGER DEFAULT 85,
	"score_good"	INTEGER DEFAULT 65,
	PRIMARY KEY("id")
);

DROP TABLE IF EXISTS "testing";
CREATE TABLE IF NOT EXISTS "testing" (
	"id"	INTEGER,
	"ts"	TEXT DEFAULT CURRENT_TIMESTAMP,
	"user$id"	INTEGER,
	"test$id"	INTEGER,
	"grade"	INTEGER DEFAULT NULL,
	FOREIGN KEY("user$id") REFERENCES "user"("id"),
	FOREIGN KEY("test$id") REFERENCES "test"("id"),
	PRIMARY KEY("id")
);

DROP TABLE IF EXISTS "answer";
CREATE TABLE IF NOT EXISTS "answer" (
	"ts"	TEXT DEFAULT CURRENT_TIMESTAMP,
	"answers"	TEXT,
	"valid"	INTEGER DEFAULT 0,
	"question$id"	INTEGER,
	"testing$id"	INTEGER,
	FOREIGN KEY("question$id") REFERENCES "question"("id"),
	FOREIGN KEY("testing$id") REFERENCES "testing"("id")
);

-- создаем группу админов
INSERT INTO "grp" ("name") VALUES ("Admins");
-- и пользователя admin:123
INSERT INTO "user" ("name","password","grp$id") VALUES ("admin", "202cb962ac59075b964b07152d234b70", 1);
-- создаем тест длительностью 2 минуты и с названием "Простой тест"
INSERT INTO "test" ("name","duration") VALUES ("Простой тест", 2);
-- добавим в этот тест 3 вопроса разных типов:
-- 1) с одним вариантом выбора
-- 2) с точным указанием ответа
-- 3) с несколькими вариантами ответа
INSERT INTO "question" ("text","picture","options","valid","type","test$id") VALUES
 ('Вопрос с одним верным ответом',NULL,'Не правильный ответ
Такой же как предыдущий ответ
Верный ответ
Совсем неверный ответ','0,0,1,0',1,1),
 ('Напишите слово ОТВЕТ',NULL,'','ОТВЕТ',2,1),
 ('Вопрос с несколькими правильными ответами',NULL,'Правильный ответ
Не правильный ответ
Такой же как первый
Такой же как второй ответ','1,0,1,0',0,1);

COMMIT;
