#ifndef KCHECKSERVER_H
#define KCHECKSERVER_H

#include <QTcpServer>
#include <QMap>
#include <QMutex>
#include <QTimer>

class QSqlDatabase;
struct KCheckClientContext;

class KCheckServer : public QTcpServer {
    Q_OBJECT

public:
    KCheckServer(QObject * obj = nullptr);
    ~KCheckServer();

private slots:
    void slotConnect();
    void slotDisconnect();
    void slotRead();

    void slotCleanupTimer();

private:
    QSqlDatabase * mDB;
    QTimer mCleanupTimer;

    QMap<QTcpSocket*, KCheckClientContext*> mClients;
    QMutex mClientsMutex;

    bool doResult(KCheckClientContext * pCtx);
    bool doTesting(KCheckClientContext * pCtx);
    bool doLogin(KCheckClientContext * pCtx);
    bool doTestPick(KCheckClientContext * pCtx);
};

#endif // KCHECKSERVER_H
