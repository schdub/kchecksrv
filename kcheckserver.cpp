#include "kcheckserver.h"

#include <QScopedPointer>
#include <QMutexLocker>
#include <QByteArray>
#include <QDataStream>
#include <QDateTime>
#include <QTime>
#include <QTcpSocket>

#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>

#include <QFile>

#define TRACE() qDebug() << __FUNCTION__

const int INTERVAL_CLEANUP_TIMER = ( 5 * 1000);
const int INTERVAL_LOGIN         = ( 2 * 1000);
const int INTERVAL_TESTPICK      = (20 * 1000);
const int INTERVAL_RESULT        = (20 * 1000);

// тип вопроса
enum QuestionType {
    qestionTypeMulty,
    qestionTypeSingle,
    qestionTypeEcsactly
};

// состояние клиента
enum KCheckClientState {
    stateLogin,
    stateTestPick,
    stateTesting,
    stateResult,
    stateFailed
};

struct KCheckClientContext {
    QTime mTSLast;            // последнее чтение из данного сокета
    QTime mTSStart;           // время начала тестирования

    QTcpSocket *mSocket;      // сокет клиента
    KCheckClientState mState; // состояние данного соединения
    QByteArray mBuff;         // временный буфер

    QVector<int> mQuestions;  // отданные данному клиенту вопросы
    QVector<QString> mValids; // валидные значения
    QVector<int> mVals;       // отмеченные результаты
    QVector<int> mTypes;      // типы каждого вопроса

    int mUserId;              // ид пользователя в БД
    int mTestingId;           // ид тестирования в БД
    int mDuration;            // продолжительность тестирования

    int mScoreExcellent;      // оценка отлично, значение от [mScoreGood, 100)
    int mScoreGood;           // оценка хорошо, значение от (0, mScoreExcellent]

    KCheckClientContext()
        : mUserId(-1)
        , mTestingId(-1)
        , mDuration (0)
        , mScoreExcellent(85)
        , mScoreGood(65)
    {}

    ~KCheckClientContext() {
        if (mSocket) {
            mSocket->close();
            mSocket->deleteLater();
            mSocket = nullptr;
        }
    }

    void sendError(const QString & msg) {
        Q_ASSERT(mSocket);

        mState = stateFailed;

        QByteArray b = "{\"e\":\"" + msg.toUtf8() + "\"}";
        mSocket->write(b);
        mSocket->flush();
    }

    static QString loadPicture(const QString & picValue) {

        if (!picValue.startsWith("file://")) {
            return picValue;
        } else {
            QString fn(picValue);
            QFile f(fn.remove("file://"));
            if (!f.open(QIODevice::ReadOnly)) {
                qWarning() << "can't open" << fn;
            } else {
                return QString(f.readAll().toBase64()).remove('\n');
            }
        }

        return QString();
    }
};

// ///////////////////////////////////////////////////////////////////////// //

KCheckServer::KCheckServer(QObject *obj)
    : QTcpServer(obj)
{
    mDB = new QSqlDatabase(QSqlDatabase::addDatabase("QSQLITE"));
    mDB->setDatabaseName("kcheck.db");
    if (!mDB->open()) {
        qDebug() << mDB->lastError().text();
        return;
    }

    connect(this, SIGNAL(newConnection()), this, SLOT(slotConnect()));

    connect( &mCleanupTimer, SIGNAL(timeout()), this, SLOT(slotCleanupTimer()) );
    mCleanupTimer.setInterval(INTERVAL_CLEANUP_TIMER);
    mCleanupTimer.start();
}

// ///////////////////////////////////////////////////////////////////////// //

KCheckServer::~KCheckServer()
{}

// ///////////////////////////////////////////////////////////////////////// //

void KCheckServer::slotCleanupTimer() {
    TRACE();

    // этот контекст будет удален
    QScopedPointer<KCheckClientContext> pCtx;

    // ищем клиента с ошибочным состоянием
    {
        QMutexLocker lock(&mClientsMutex);
        auto v = mClients.values();
        for (auto vi = v.begin(), ve = v.end(); vi < ve; ++vi) {
            if ((*vi)->mState == stateFailed) {
                // удаляем его из списка активных клиентов
                pCtx.reset(*vi);
                mClients.remove(pCtx->mSocket);
                break;
            }
        }
    }

    // попробуем поискать неактивных клиентов
    // для начала только тех, кто подключился, но логина не произошло

    if (pCtx.isNull()) {
        QMutexLocker lock(&mClientsMutex);
        for (;;) {
            bool wasDeletions = false;
            auto v = mClients.values();
            for (auto vi = v.begin(), ve = v.end(); vi < ve; ++vi) {
                KCheckClientContext* ctx = *vi;
                if (ctx->mState == stateLogin) {
                    if (ctx->mTSLast.elapsed() >= INTERVAL_LOGIN) {
                        qDebug()<< "del by LOGIN interval";
                        ctx->mSocket->disconnect();
                        mClients.remove(ctx->mSocket);
                        delete ctx;
                        wasDeletions = true;
                        break;
                    }
                }
            }
            // выходим, т.к. не было ничего удалено
            if (!wasDeletions) break;
        }
    }

    // теперь тех, кто отвалился на других этапах

    if (pCtx.isNull()) {
        QMutexLocker lock(&mClientsMutex);
        auto v = mClients.values();
        for (auto vi = v.begin(), ve = v.end(); vi < ve; ++vi) {
            KCheckClientContext * ctx = *vi;
            if (ctx->mState == stateTestPick) {
                if (ctx->mTSLast.elapsed() >= INTERVAL_TESTPICK) {
                    qDebug()<< "del by TESTPICK interval";
                    pCtx.reset(ctx);
                    pCtx->mSocket->disconnect();
                    mClients.remove(pCtx->mSocket);
                    break;
                }
            } else if (ctx->mState == stateTestPick) {
                if (ctx->mTSLast.elapsed() >= INTERVAL_RESULT) {
                    qDebug()<< "del by RESULT interval";
                    pCtx.reset(ctx);
                    pCtx->mSocket->disconnect();
                    mClients.remove(pCtx->mSocket);
                    break;
                }
            }
        }
    }
}

// ///////////////////////////////////////////////////////////////////////// //

void KCheckServer::slotDisconnect() {
    TRACE();
    QTcpSocket * pSock = qobject_cast<QTcpSocket*>(sender());
    if (!pSock) return;

    // этот контекст будет удален
    QScopedPointer<KCheckClientContext> pCtx;
    {
        QMutexLocker lock(&mClientsMutex);
        pCtx.reset(mClients[pSock]);
        mClients.remove(pSock);
    }
}

// ///////////////////////////////////////////////////////////////////////// //

void KCheckServer::slotRead() {
    TRACE();
    QTcpSocket * pSock = qobject_cast<QTcpSocket*>(sender());
    if (!pSock) return;

    KCheckClientContext * pCtx = nullptr;
    {
        QMutexLocker lock(&mClientsMutex);
        pCtx = mClients[pSock];
    }

    if (!pCtx) return;
    pCtx->mTSLast.start();

    QByteArray arr = pSock->readAll();
    pCtx->mBuff += arr;

    if (pCtx->mBuff.size() >= 100 * 1024) {
        qWarning() << "client traffic limit reached" << pCtx->mUserId;
        pCtx->sendError("invalid client");
        return;
    }

    // TODO: validate JSON here, before actual parsing!!!

    switch (pCtx->mState) {
    case stateLogin:    doLogin(pCtx);    break;
    case stateTestPick: doTestPick(pCtx); break;
    case stateTesting:  doTesting(pCtx);  break;
    case stateResult:   doResult(pCtx);   break;
    case stateFailed:                     break;
    }
}
// ///////////////////////////////////////////////////////////////////////// //

void KCheckServer::slotConnect() {
    TRACE();

    QTcpSocket * pSock = nextPendingConnection();

    if (!pSock) {
        qDebug() << "!pSock";
        return;
    }

    connect(pSock, SIGNAL(readyRead()), this, SLOT(slotRead()));
    connect(pSock, SIGNAL(disconnected()), this, SLOT(slotDisconnect()));

    KCheckClientContext * pCtx = new KCheckClientContext;
    pCtx->mSocket = pSock;
    pCtx->mState = stateLogin;
    pCtx->mTSLast.start();

    {
        QMutexLocker lock(&mClientsMutex);
        mClients[pSock] = pCtx;
    }
}

// ///////////////////////////////////////////////////////////////////////// //

bool KCheckServer::doLogin(KCheckClientContext *pCtx) {
    TRACE();

    QJsonDocument doc = QJsonDocument::fromJson( pCtx->mBuff );
    if (doc.isEmpty()) {
        return false;
    }

    QString login = doc["l"].toString();
    QString pass  = doc["p"].toString();
    if (login.isEmpty() || pass.isEmpty()) {
        pCtx->sendError("Invalid request");
        return false;
    }

    pCtx->mBuff.clear();

    bool ok = true;

    // проверяем пару логин/пароль, что есть такие в БД

    if (ok) {
        QSqlQuery query(*mDB);
        query.prepare("SELECT id FROM user WHERE name=:name AND password=:pass AND blocked=0");
        query.bindValue(":name", login);
        query.bindValue(":pass", pass);
        ok = query.exec();
        if (query.next()) {
            pCtx->mUserId = query.value(0).toInt();
        } else {
            pCtx->sendError("Invalid login or password");
            return false;
        }
    }

    // формируем список тестов для выбора пользователем

    if (ok) {
        pCtx->mState = stateTestPick;

        QSqlQuery query(*mDB);
        query.prepare("SELECT * FROM test");
        ok = query.exec();

        if (ok) {
            QString r = "[";
            int cnt = 0;
            while (query.next()) {
                if (cnt) r += ",";
                r += QString("{\"id\":%1, \"name\":\"%2\", \"duration\":%3}")
                        .arg(query.value(0).toInt())
                        .arg(query.value(1).toString())
                        .arg(query.value(2).toInt());
                ++cnt;
            }
            r += "]";
            pCtx->mSocket->write(r.toUtf8());
        }
    }

    if (!ok) {
        // произошла ошибка при общении с базой данных
        qWarning() << mDB->lastError().text();
        pCtx->sendError("DB error");
        return false;
    }

    return true;
}

// ///////////////////////////////////////////////////////////////////////// //

bool KCheckServer::doTestPick(KCheckClientContext *pCtx) {
    TRACE();

    QJsonDocument doc = QJsonDocument::fromJson( pCtx->mBuff );
    if (doc.isEmpty()) {
        return false;
    }

    int testId = doc["id"].toInt();
    if (testId == 0) {
        pCtx->sendError("Invalid request");
        return false;
    }

    pCtx->mBuff.clear();

    bool ok = true;

    if (ok) {
        // получаем ИД конкретного тестирования
        QSqlQuery query(*mDB);
        query.prepare("INSERT INTO testing(user$id, test$id) VALUES(:userId, :testId)");
        query.bindValue(":userId", pCtx->mUserId);
        query.bindValue(":testId", testId);
        ok = query.exec();

        if (ok) {
            pCtx->mTestingId = query.lastInsertId().toInt();
        } else {
            // произошла ошибка при общении с базой данных
            qWarning() << query.lastError().text();
            pCtx->sendError("DB error: " + query.lastError().text());
            return false;
        }
    }

    if (ok) {
        QSqlQuery query(*mDB);
        query.prepare("SELECT duration, score_excellent, score_good FROM test WHERE id=:id");
        query.bindValue(":id", testId);
        ok = query.exec();

        if (!query.next()) {
            pCtx->sendError("Invalid test$id");
            return false;
        } else {
            pCtx->mDuration = query.value(0).toInt();
            pCtx->mScoreExcellent = query.value(1).toInt();
            pCtx->mScoreGood = query.value(2).toInt();
        }
    }

    // выбираем все вопросы данного теста

    if (ok) {
        pCtx->mState = stateTesting;

        QSqlQuery query(*mDB);
        query.prepare("SELECT id, text, options, valid, type, picture FROM question where test$id=:id");
        query.bindValue(":id", testId);
        ok = query.exec();

        if (!ok) {
            pCtx->sendError("Invalid test$id");
            return false;
        }

        // формируем вопросы для данного теста, чтобы отдать клиенту

        if (ok) {

            int cnt = 0;
            QString r = "{ \"dur\":" + QString::number(pCtx->mDuration) + ", \"a\":[";

            while (query.next()) {
                if (cnt) r += ",";

                pCtx->mVals.push_back(-1);
                pCtx->mValids.push_back(query.value(3).toString());
                pCtx->mQuestions.push_back(query.value(0).toInt());
                pCtx->mTypes.push_back(query.value(4).toInt());

                r += "{";

                r += QString("\"Text\":\"%1\"").arg(query.value(1).toString());

                r += QString(", \"Type\":%1").arg(query.value(4).toInt());

                const QString & pic = KCheckClientContext::loadPicture( query.value(5).toString() );
                if (!pic.isEmpty()) {
                    r += ", \"Picture\":\"";
                    r += pic;
                    r += "\"";
                }

                r += ", \"Items\": [";
                QStringList items = query.value(2).toString().split('\n');
                for (int i = 0; i < items.size(); ++i) {
                    if (i) r += ",";
                    r += QString("\"%1\"").arg(items[i]);
                }

                r += "]}";

                ++cnt;
            }
            r += "]}";
            pCtx->mSocket->write(r.toUtf8());
        }

    }

    return true;
}

// ///////////////////////////////////////////////////////////////////////// //

bool KCheckServer::doTesting(KCheckClientContext * pCtx) {
    TRACE();

    QJsonDocument doc = QJsonDocument::fromJson( pCtx->mBuff );
    if (doc.isEmpty()) {
        return false;
    }

    if ( !doc["ts"].isUndefined() ) {
        return doResult(pCtx);
    }

    const QJsonArray & arr = doc["a"].toArray();
    if (arr.isEmpty()) {
        pCtx->sendError("Invalid request");
        return false;
    }

    int questionIndex = doc["i"].toInt();
    if (questionIndex < 0 || questionIndex >= pCtx->mQuestions.size()) {
        pCtx->sendError("Invalid questionIndex");
        return false;
    }

    pCtx->mBuff.clear();

    int questionId = pCtx->mQuestions[questionIndex];
    int isValid = 0;
    QString actualString = "";
    {
        int questionType = pCtx->mTypes[questionIndex];
        if (questionType == qestionTypeMulty ||
            questionType == qestionTypeSingle ) {
            // формируем текущий ответ для типов вопросов с одним выбором ответа
            // и несколькими вариантами ответов
            if (arr.size() > 0)
                actualString += QString::number(arr[0].toInt());
            for (int i = 1;  i < arr.size(); ++i) {
                actualString += ",";
                actualString += QString::number(arr[i].toInt());;
            }
        } else if (questionType == qestionTypeEcsactly) {
            // у нас тип вопроса с указанием точного ответа
            if (arr.size() > 0)
                actualString = arr[0].toString();
        }
        // берем верный ответ
        const QString & validString = pCtx->mValids[questionIndex];

        // сравниваем
        isValid = (actualString.compare(validString) == 0);

        // сохраняем у себя в контекстеы
        pCtx->mVals[questionIndex] = isValid;
    }

    bool ok = true;

    // запишем ответ

    if (ok) {
        QSqlQuery query(*mDB);
        query.prepare("REPLACE INTO answer(answers, valid, question$id, testing$id) "
                      " VALUES(:answers, :valid, :questionId, :testingId)");
        query.bindValue(":answers", actualString);
        query.bindValue(":valid", isValid);
        query.bindValue(":questionId", questionId);
        query.bindValue(":testingId", pCtx->mTestingId);
        ok = query.exec();

        if (!ok) {
            // произошла ошибка при общении с базой данных
            qWarning() << query.lastError().text();
            pCtx->sendError("DB error: " + query.lastError().text());
            return false;
        }

    }

    return true;
}

// ///////////////////////////////////////////////////////////////////////// //

bool KCheckServer::doResult(KCheckClientContext * pCtx) {
    TRACE();

    QJsonDocument doc = QJsonDocument::fromJson( pCtx->mBuff );
    if (doc.isEmpty()) {
        return false;
    }

    QString timeStamp = doc["ts"].toString();
    if (timeStamp.isEmpty()) {
        pCtx->sendError("Invalid request");
        return false;
    }

    pCtx->mBuff.clear();
    pCtx->mState = stateResult;

    int correct = 0;
    int incorrect = 0;
    int unanswered = 0;
    int total = 0;

    for (int i = 0; i < pCtx->mVals.size(); ++i) {
        total += 1;
        switch (pCtx->mVals[i]) {
        case 0: incorrect   += 1; break;
        case 1: correct     += 1; break;
        default: unanswered += 1; break;
        }
    }

    int grade = 0;
    float score = (correct * 100.0) / total;
    if (score >= pCtx->mScoreExcellent) {
        grade = 2;
    } else if (score >= pCtx->mScoreGood)  {
        grade = 1;
    }

    {
        bool ok;
        QSqlQuery query(*mDB);
        query.prepare("UPDATE testing SET grade=:grade WHERE id=:testId");
        query.bindValue(":grade", grade);
        query.bindValue(":testId", pCtx->mTestingId);
        ok = query.exec();
        if (!ok) {
            qWarning() << "can't UPDATE grade" << pCtx->mTestingId;
        }
    }

    QJsonObject obj;
    obj["correct"] = correct;
    obj["incorrect"] = incorrect;
    obj["unanswered"] = unanswered;
    obj["grade"] = grade;
    pCtx->mSocket->write( QJsonDocument(obj).toJson() );

    return true;
}

// ///////////////////////////////////////////////////////////////////////// //
