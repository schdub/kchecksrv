#ifndef MAINDIALOG_H
#define MAINDIALOG_H

#include <QDialog>
#include "kcheckserver.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainDialog; }
QT_END_NAMESPACE

class MainDialog : public QDialog
{
    Q_OBJECT

public:
    MainDialog(QWidget *parent = nullptr);
    ~MainDialog();

private:
    Ui::MainDialog *ui;
    KCheckServer mServer;
};
#endif // MAINDIALOG_H
